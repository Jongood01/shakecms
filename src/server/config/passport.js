'use strict';

module.exports = function(passport, FacebookStrategy, GoogleStrategy, TwitterStrategy) {
    var User = require('../models/User');
    
    passport.use(new FacebookStrategy({
        clientID: '222621914745768',
        clientSecret: '9febce360c7a9752e18eb20fec658476',
        callbackURL: "http://localhost:3000/auth/facebook/callback",
        enableProof: true,
        profileFields: ['emails', 'first_name', 'last_name']
    },
    function(accessToken, refreshToken, profile, cb) {
        User.findOne({facebookId: profile.id}, function(err, user) {
            if(err) {
                return cb(err);
            }
            if(user) {
                return cb(null, user);
            } else {
                var newUser = new User(); 
                newUser.facebookId = profile.id;
                newUser.firstname = profile.name.givenName;
                newUser.lastname = profile.name.familyName;
                newUser.email = profile.emails[0].value; 
                newUser.token = accessToken;
                newUser.save(function(err, user) { 
                    cb(err, user);         
                });               
            }
        });
    }
    ));
    
    passport.serializeUser(function(user, done) {
        return done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
           return done(err, user);
        });
    });   
    
    passport.use(new GoogleStrategy({
        clientID: '850502730094-mpm3psh2180398smedd1df5haphkakil.apps.googleusercontent.com',
        clientSecret: 'Ro0EDuYGW-V8kpdimpc6VZTn',
        callbackURL: "http://localhost:3000/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, cb) {
        User.findOne({ googleId: profile.id }, function (err, user) {
            if(err) {
                return cb(err);
            }
            if(user) {
                return cb(null, user);
            } else {
                console.log(profile);
                var newUser = new User(); 
                newUser.googleId = profile.id;
                newUser.firstname = profile.name.givenName;
                newUser.lastname = profile.name.familyName;
                newUser.email = profile.emails[0].value; 
                newUser.token = accessToken;
                newUser.save(function(err, user) { 
                    return cb(err, user);         
                });               
            }
        });
    }
    ));
    
    passport.use(new TwitterStrategy({
        consumerKey: 'URGohhAIV8NVa2tCY2ogAbTqU',
        consumerSecret: 's19UigDdnYke4AQrJvBnAzcDEcXw71aJdZRCiHryY094jOO67q',
        callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    function(token, tokenSecret, profile, cb) {
        User.findOne({ twitterId: profile.id }, function (err, user) {
            if(err) {
                return cb(err);
            }
            if(user) {
                return cb(null, user);
            } else {
                var names = profile.name.split(" ");
                var firstname = '';
                var lastname = '';
                if(names.length === 2) {
                    firstname = names[0];
                    lastname = names[1];
                } else {
                    
                }
                console.log(profile);
                var newUser = new User(); 
                newUser.twitterId = profile.id;
                newUser.firstname = firstname;
                newUser.lastname = lastname;
                newUser.email = profile.emails[0].value; 
                newUser.token = accessToken;
                newUser.save(function(err, user) { 
                    return cb(err, user);         
                });               
            }
        });
    }
    ));
}