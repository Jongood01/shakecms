var mongoose = require('mongoose');
var Schema = mongoose.Schema;
'use strict';

mongoose.connect('mongodb://localhost/shake-cms'); 

var userSchema = new Schema({
    firstname: String,
    lastname: String,
    username: String,
    email: String,
    imageUrl: String,
    facebookId: String,
    googleId: String,
    twitterId: String,
    token: String
});

module.exports = mongoose.model('User', userSchema);