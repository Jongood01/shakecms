import {Pipe} from 'angular2/core';

@Pipe({
  name: 'search'
})
export class SearchPipe {
  transform(value, [term, key]) {
    term = term || '';
    value = value || [];
    return value.filter((val) => val[key].toUpperCase().startsWith(term.toUpperCase()));
  }
}
