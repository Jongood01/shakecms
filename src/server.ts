import * as path from 'path';
import * as express from 'express';
import mongoose = require('mongoose');

// Angular 2
import {ng2engine, REQUEST_URL, NODE_LOCATION_PROVIDERS} from 'angular2-universal-preview';
import {provide, enableProdMode} from 'angular2/core';
import {APP_BASE_HREF, ROUTER_PROVIDERS} from 'angular2/router';
import {App} from './app/app';

let app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let root = path.join(path.resolve(__dirname, '..'));
var session = require('express-session');
var jwt = require('jsonwebtoken');
var jwtConfig = require('./server/config/jwt');
var socketJWT = require('socketio-jwt');
var passport = require('passport'); 
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
app.set('JWTSecret', jwtConfig.secret);
var auth = require('./server/config/passport')(passport, FacebookStrategy, GoogleStrategy, TwitterStrategy);

enableProdMode();

// Express View
app.engine('.html', ng2engine);
app.set('views', __dirname);
app.set('view engine', 'html');

function ngApp(req, res) {
  let baseUrl = '/';
  let url = req.originalUrl || '/';
  res.render('index', {
    App,
    providers: [
      provide(APP_BASE_HREF, {useValue: baseUrl}),
      provide(REQUEST_URL, {useValue: url}),
      ROUTER_PROVIDERS,
      NODE_LOCATION_PROVIDERS,
    ],
    preboot: true
  });
}

// Serve static files
app.use(express.static(root));

var usersNSP = io.of('/users');
usersNSP.on('connection', socketJWT.authorize({
    secret: jwtConfig.secret,
    timeout: 15000 // 15 seconds to send the authentication message
  })).on('authenticated', function(socket) {
    //this socket is authenticated, we are good to handle more events from it.
    require('./server/sockets/users')(socket);
  });
//usersNSP.on('connection', function(socket) { require('./server/sockets/users')(socket) });

// Routes
app.get('/auth/facebook',
  passport.authenticate('facebook', {session: false, scope: ['email']}));

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', {session: false, failureRedirect: '/login' }),
  function(req, res) { 
    var token = jwt.sign(req.user, app.get('JWTSecret'), {
        expiresIn: 86400 // expires in 24 hours
    });
    var jwtNSP = io.of('/jwt');
    jwtNSP.on('connection', function (socket) {
       socket.emit('/test', {test: token}); 
    });
    res.redirect('/auth');
});

app.get('/auth/google', passport.authenticate('google', {session: false, scope: ['email']}));

app.get('/auth/google/callback',
  passport.authenticate('google', {session: false, failureRedirect: '/login' }), 
  function(req, res) {
    var token = jwt.sign(req.user, app.get('JWTSecret'), {
        expiresIn: 86400 // expires in 24 hours
    });
    res.redirect('/auth');                                
  });
  
 app.get('/auth/twitter', passport.authenticate('twitter', {session: false, scope: ['email']}));

app.get('/auth/twitter/callback',
  passport.authenticate('twitter', {session: false, failureRedirect: '/login' }), 
  function(req, res) {
    var token = jwt.sign(req.user, app.get('JWTSecret'), {
        expiresIn: 86400 // expires in 24 hours
    });
    res.redirect('/auth');                                
  });

app.use('/', ngApp);

// Server
server.listen(3000, () => {
  console.log('Listen on http://localhost:3000');
});
