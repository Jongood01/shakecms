import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {ConfigService} from './config-service';

@Injectable()
export class CoreService {

	private _endpoint: string;

	constructor(
		private _config: ConfigService,
		private _http: Http
	) {
		this._endpoint = this._config.shakeApiRoot + 'install';
		
	}

	install() {
		return this._http.get(this._endpoint)
			.map(res => res.json())
            .catch(this.handleError);
	}

	handleError(error: Response) {
		return Observable.throw(error || 'Server error');
	}

}
