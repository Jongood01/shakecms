import {Injectable} from 'angular2/core';

@Injectable()
export class FacebookLoginService {
    constructor() {
        if(typeof window !== 'undefined') {
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '222621914745768',
                    cookie     : true,  // enable cookies to allow the server to access 
                    xfbml      : true,  // parse social plugins on this page
                    version    : 'v2.5' // use graph api version 2.5
                });
            };              
        } 
    }
    
    onNgInit() {
       
    }
    
    doLogin() {
         FB.login(function(response) {
        // handle the response
        console.log(response);
        }, {scope: 'public_profile,email'});
    }
}