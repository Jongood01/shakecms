import {Injectable, Inject} from 'angular2/core';
import {Router} from 'angular2/router';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from './config-service';

@Injectable()
export class AuthService {
    public isLoggedIn: Observable<any>;
    firebaseUrl: string;
    uid: string = null;
    authData: any;
    public authRef: any;
    constructor(
        private _config: ConfigService,
        private _router: Router
    ) {
        
    }
    getProfile(authData: any) {
        var profile = { name: '', image: '' };
        switch (authData.provider) {
            case 'google':
                profile.name = authData.google.displayName;
                profile.image = authData.google.profileImageURL;
            case 'facebook':
                profile.name = authData.facebook.displayName;
                profile.image = authData.facebook.profileImageURL;
        }
        return profile;
    }
    authHandler(error: any, authData: any) {
        if (error) {
            console.log("Login Failed!", error);
        } else {

            //this.redirectToHome();
        }
    }
    newUserHandler(error: any, userData: any) {
        if (error) {
            console.log("Error creating user:", error);
        } else {
            return userData.uid;
        }
    }
    createUser(email: string, password: string) {
        return this.authRef.createUser({
            email: email,
            password: password
        }, this.newUserHandler);
    }
    authWithPassword(email: string, password: string) {
        this.authRef.authWithPassword({
            email: email,
            password: password
        }, this.authHandler);
    }
    authWithFacebook() {
        this.authRef.authWithOAuthPopup('facebook', this.authHandler);
    }
    authWithGoogle() {
        this.authRef.authWithOAuthPopup('google', this.authHandler);
    }
    redirectToHome() {
        console.log('we got here');
        //this._router.navigate(['/Accounts']);
    }
    logout() {
        this.authRef.unauth();
    }
}
