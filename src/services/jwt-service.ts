
export class JwtService {
	public token: string;
	constructor() {
        if(typeof window === 'undefined') {
           this.token = ''; 
        } else {
           this.token = localStorage.getItem('shakeJWT') || '';
        }
	}
}
