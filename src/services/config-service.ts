
export class ConfigService {
	public firebaseRoot: string;
	public environment: string;
	public shakeApiRoot: string;
	constructor() {
		// Staging data
	    this.firebaseRoot = 'https://shakecms-staging.firebaseio.com';
	    //Production data
		//this.firebaseRoot = 'https://shakecms.firebaseio.com'; 
		
		this.shakeApiRoot = 'http://localhost/shake-service/public/api/v1/';
	}
}
