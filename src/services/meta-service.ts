import {Injectable} from 'angular2/core';
import {BrowserDomAdapter} from 'angular2/platform/browser';

@Injectable()
export class MetaService {

	public document: any;
	public headElement: any;

	constructor(
		private _domAdapter: BrowserDomAdapter
	) {
		this.headElement = document.getElementsByTagName('head')[0];
	}

	setTitle(value: string) {
		this._domAdapter.setTitle(value);
	}
	setDescription(value: string) {
		this.addMeta('description', value);
	}
	createMeta(key: string, content: string, attribute: string = 'name') {
		var meta = document.createElement('meta');
		meta[attribute] = key;
		meta.content = content;
		return meta;
	}
	addMeta(key: string, content: string, attribute: string = 'name') {
		this.headElement.appenChild(this.createMeta(key, content, attribute));
	}
	addOpenGraphMeta(graph: Object) {
		//TODO
		//Add code here to create OG tags and Twitter card tags - maybe 2 sepaate methods
	}
}
