import {Component, Inject} from 'angular2/core';
import {Observable} from 'rxjs';

import { Router, RouteConfig, ROUTER_DIRECTIVES, RouteParams } from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ConfigService} from '../services/config-service';
import {CoreService} from '../services/core-service';

@Component({
    selector: 'page',
    template: `
    <div class="row">
        <div class="small-12 medium-10 medium-centered columns">
            {{title}}
            {{content}}
            <button class="button" (click)="createTest()">click me</button> 
        </div>
    </div>
  `,
    styles: [require('./page.scss').toString()],
    directives: [ROUTER_DIRECTIVES],
    providers: [CoreService, ConfigService, HTTP_PROVIDERS]
})

export class Page {
    
    private _ref: any;
    public title: string = '';
    public content: string = '';
    public token: string = '';
    private _routeParams:RouteParams

    constructor(
        public config: ConfigService,
        public core: CoreService
    ) {
        
    }

    
    load() {
        
    }
    createTest() {
   
 }
}
