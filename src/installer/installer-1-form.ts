import {Component, Input} from 'angular2/core';
import {ChangeDetectionStrategy} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {ConfigService} from '../services/config-service';
import {AuthService} from '../services/auth-service';

@Component({
  selector: 'installer-1-form',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <h3>Create an account</h3>
      <form (ngSubmit)="onSubmit()" #installer1Form="ngForm">
      <div class="row">
        <div class="small-12 columns">
          <label>E-mail:</label>
          <input type="email" [(ngModel)]="email">
        </div>
      </div>
      <div class="row">
        <div class="small-12 columns">
          <label>Password:</label>
          <input type="password" [(ngModel)]="password">
        </div>
      </div>
      <input type="submit" class="button small" value="Create account">
      </form>
  `,
  providers: [AuthService],
  directives: []
})

export class Installer1Form {
    @Input() installData;
    public uid: string;

    public email: string;
    public password: string;

    constructor(
      public auth: AuthService
    ) {
        
    }
    ngOnInit() {
      
    }
    onSubmit() {
      this.uid = this.auth.createUser(this.email, this.password);
      this.installData.uid = 'wysiwyg';
    }

}
