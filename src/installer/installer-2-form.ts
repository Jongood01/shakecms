import {Component, Input} from 'angular2/core';
import {ChangeDetectionStrategy} from 'angular2/core';
import {NgForm} from 'angular2/common';
import {ConfigService} from '../services/config-service';
import {AuthService} from '../services/auth-service';

@Component({
  selector: 'installer-2-form',
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <h3>Basic setup</h3>
      <form (ngSubmit)="onSubmit()" #installer1Form="ngForm">
      <div class="row">
        <div class="small-12 columns">
          <label>Site name:</label>
          <input type="text" [(ngModel)]="installData.name">
        </div>
      </div>
      <div class="row">
        <div class="small-12 columns">
          <label>Description:</label>
          <textarea [(ngModel)]="installData.description"></textarea>
        </div>
      </div>
      </form>
  `,
  directives: []
})

export class Installer2Form {
    @Input() installData;

    constructor(
      public auth: AuthService
    ) {

    }
    login(type: string) {
      if (type === 'facebook') {
        this.auth.authWithFacebook();
      } else {
        this.auth.authWithGoogle();
      }      
    }

    ngOnInit() {

    }
    onSubmit() {
   
    }

}
