import {Component} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {CoreService} from '../services/core-service';
import {Installer1Form} from './installer-1-form';
import {Installer2Form} from './installer-2-form';

@Component({
    selector: 'installer',
    template: `
<div class="row">
	<div class="small-12 medium-10 medium-centered large-6 columns">
		<h2>Install new site</h2>
		<installer-1-form *ngIf="step == 1" [(installData)]="installData"></installer-1-form>
		<installer-2-form *ngIf="step == 2" [(installData)]="installData"></installer-2-form>

		<button *ngIf="step > 1" class="small" (click)="previous()"><i class="fi-arrow-left"></i> Prev</button> 
		<button class="small" (click)="next()">Next <i class="fi-arrow-right"></i></button>		
	</div>
</div>  
    `,
    directives: [ROUTER_DIRECTIVES, Installer1Form, Installer2Form]
})
export class InstallerComponent {
    public installData: Object;
    public step: number = 1;

    constructor(
        private _router: Router,
        private _core: CoreService
    ) {
        this.installData = { name: 'test person' };

    }
    ngOnInit() {

    }

    next() {
        this.step++;
    }

    previous() {
        this.step--;
    }

}
