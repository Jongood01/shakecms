import {Directive, OnDestroy} from 'angular2/core';
import {Router, Location} from 'angular2/router';
import {AuthService} from '../services/auth-service';

@Directive({
    selector:'[authFilter]'
})

export class AuthFilterDirective implements OnDestroy {
    private _sub: any = null;
    constructor(
        private _auth: AuthService,
        private _location: Location,
        private _router: Router
    ) {

    }
    ngOnInit() {
        if(!this._auth.uid) {
            this._location.replaceState('/');
            this._router.navigate(['Login']);
        }

        this._sub = this._auth.isLoggedIn.subscribe((val) => {
            if(!val) {
                this._location.replaceState('/');
                this._router.navigate(['Login']);
            }
        });
    }
    ngOnDestroy() {
        if (this._sub !== null) {
            this._sub.unsubscribe();
        }
    }
}
