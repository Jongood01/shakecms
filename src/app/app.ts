import {Component, ViewEncapsulation} from 'angular2/core';
import {Observable} from 'rxjs';
import * as io from 'socket.io-client';

import { Router, RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {ConfigService} from '../services/config-service';
import {CoreService} from '../services/core-service';
//import {LoginComponent} from '../login/login';
import { Page } from '../page/page';
import {InstallerComponent} from '../installer/installer';
import { Admin } from '../admin/admin';
import { AuthComponent } from '../auth/auth';
import { AdminPanel } from '../admin/admin-panel';
import { JwtService } from '../services/jwt-service';
import {FacebookLoginService} from '../services/facebook-login-service';

@Component({
    selector: 'app',
    template: `
  <header>
  <admin-panel *ngIf="showAdminMenu"></admin-panel>
    <a [routerLink]="['/Install']">Install</a>
    <section class="row">
    <nav class="small-12 medium-10 medium-centered columns">
        <div class="row">
            <div class="small-12 columns">
                <h1>Shake CMS</h1>
                {{test | json}}
                JWT is {{testjwt | json}}
                <button (click)="facebookLogin()" >Log in with Facebook</button>
                <a href="/auth/google">Log in with Google</a>
                <a href="/auth/twitter">Log in with Twitter</a>
            </div>
        </div>
    </nav>
    </section>
    </header>
    <div class="row">
        <div class="content small-12 medium-10 medium-centered columns">
            <router-outlet></router-outlet>
        </div>
    </div>
  `,
    styles: [require('./app.scss').toString()],
    encapsulation: ViewEncapsulation.None,
    directives: [ROUTER_DIRECTIVES, AdminPanel],
    providers: [CoreService, ConfigService, HTTP_PROVIDERS, JwtService, FacebookLoginService]
})
@RouteConfig([
    { path: '/install', name: 'Install', component: InstallerComponent },
    { path: '/admin/...', name: 'Admin', component: Admin },
    { path: '/auth/:token', name: 'Auth', component: AuthComponent},
    { path: '/:slug', name: 'Page', component: Page, useAsDefault: true },     
])
export class App {
    
    public showAdminMenu: boolean = true;
    public obs: Observable<any>;
    public test: string;
    public socket: any;
    public testjwt: any;

    constructor(
        private _router: Router,
        public config: ConfigService,
        public core: CoreService,
        public jwt: JwtService,
        public facebookLoginService: FacebookLoginService
    ) {
          this.socket = io.connect('http://localhost:3000/jwt'); 
          this.obs = Observable.create(observer => { 
             this.socket.on('/test', function (data) {
                    observer.next(data);
              });
          });
        
          
        //     this.socket.on('authenticated', function (data) {

        
        //     }).emit('authenticate', {token: this.jwt.token});                   
        //   this.obs = Observable.create(observer => { 
        //      this.socket.on('/news', function (data) {
        //             observer.next(data);
        //       });
        //   });
          
          this.socket.on("error", function(error) {
            if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
                // redirect user to login page perhaps?
                console.log("Invalid auth");
            }
          });
          
          this.obs.subscribe(value => {
             this.test = value;
              
          });
  
        this._router.subscribe((url) => {
           this._router.recognize(url).then((instruction) => {
               this.showAdminMenu = (instruction.component.componentType.name === 'Admin') ? false : true;
           }); 
        });

    }
    onNgInit() {
        
    }
    facebookLogin() {
        this.facebookLoginService.doLogin();
    }
}
