import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-settings',
    template: `
    <div class="panel">
     <h4>Settings</h4>
      <div class="row">
        <div class="small-12 columns" ngRepeat="#setting in settings | async"></div>
      </div>
    </div>
  `,
    styles: [require('./settings.scss').toString()]
})
export class AdminSettings {
    public settings: Observable<any[]>;
    constructor(
    ) {
 
    }
    ngOnInit() {

    }



}
