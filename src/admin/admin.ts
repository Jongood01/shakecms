import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, RouterOutlet} from 'angular2/router';
import { AdminMenu } from './admin-menu';
import { AdminDashboard } from './dashboard/dashboard';
import { AdminUsers } from './users/users';
import { AdminSettings } from './settings/settings';
import { AdminPages } from './pages/pages';
import { AdminBlocks } from './blocks/blocks';
import { AdminMedia } from './media/media';
import { AdminCollections } from './collections/collections';

@Component({
    selector: 'admin',
    template: `
    <div class="admin">
        <div class="small-12 medium-4 large-3 columns">
            <admin-menu></admin-menu>
        </div>
        <div class="small-12 medium-8 large-9 columns">
           <router-outlet></router-outlet>
        </div>
    </div>
  `,
    styles: [require('./admin.scss').toString()],
    directives: [ROUTER_DIRECTIVES, RouterOutlet, AdminMenu, AdminDashboard, AdminUsers, AdminSettings, AdminPages, AdminBlocks, AdminMedia, AdminCollections]
})

@RouteConfig([
  {path:'/',    name: 'Dashboard',   component: AdminDashboard, useAsDefault: true},
  {path:'/users', name: 'Users', component: AdminUsers},
  {path:'/settings', name: 'Settings', component: AdminSettings},
  {path:'/pages', name: 'Pages', component: AdminPages},
  {path:'/blocks', name: 'Blocks', component: AdminBlocks},
  {path:'/media', name: 'Media', component: AdminMedia},
  {path:'/collections', name: 'Collections', component: AdminCollections}
])

export class Admin {

    public state: string;
    public view: string;
    
    constructor(
    ) {
        this.state = 'close';
    }
    ngOnInit() {

    }
}
