import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-media',
    template: `
    <div class="panel">
     <h4>Media</h4>
      <div class="row">
        <div class="small-12 columns" ngRepeat="#med in media | async"></div>
      </div>
    </div>
  `,
    styles: [require('./media.scss').toString()]
})
export class AdminMedia {
    public media: Observable<any[]>;
    constructor(
    ) {
  
    }
    ngOnInit() {

    }



}
