import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-users',
    template: `
    <div class="panel">
     <h4>Users</h4>
      <div class="row">
        <div class="small-12 columns" ngRepeat="#user in users | async"></div>
      </div>
    </div>
  `,
    styles: [require('./users.scss').toString()]
})
export class AdminUsers {
    public users: Observable<any[]>;
    constructor(
    ) {
    }
    ngOnInit() {

    }



}
