import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
    selector: 'admin-menu',
    template: `
     <div class="row">
        <div class="small-12 columns">
            <ul class="no-bullet float-left">
                <li><a [routerLink]="['Admin/Dashboard']"><i class="fi-graph-trend size-24"></i> Dashboard</a></li>
                <li><a [routerLink]="['Admin/Users']"><i class="fi-torso size-24"></i> Users</a></li>
                <li><a [routerLink]="['Admin/Settings']"><i class="fi-widget size-24"></i> Settings</a></li>
                <li><a [routerLink]="['Admin/Pages']"><i class="fi-page size-24"></i> Pages</a></li>
                <li><a [routerLink]="['Admin/Blocks']"><i class="fi-thumbnails size-24"></i> Blocks</a></li>
                <li><a [routerLink]="['Admin/Media']"><i class="fi-video size-24"></i> Media</a></li>
                <li><a [routerLink]="['Admin/Collections']"><i class="fi-folder size-24"></i> Collections</a></li>
            </ul>        
        </div>
     </div>
  `,
    styles: [require('./admin-menu.scss').toString()],
    directives: [ROUTER_DIRECTIVES]
})

export class AdminMenu {
    
    constructor(
    ) {
   
    }
    ngOnInit() {

    }



}
