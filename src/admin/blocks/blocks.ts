import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-blocks',
    template: `
    <div class="panel">
     <h4>Blocks</h4>
      <div class="row">
        <div class="small-12 columns" ngRepeat="#block in blocks | async"></div>
      </div>
    </div>
  `,
    styles: [require('./blocks.scss').toString()]
})
export class AdminBlocks {
    public blocks: Observable<any[]>;
    constructor(
    ) {
       
    }
    ngOnInit() {

    }



}
