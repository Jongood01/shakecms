import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-pages',
    template: `
    <div class="panel">
     <h4>Pages</h4>
      <div class="row">
      <button class="button">New <i class="fi-plus size-16"></i></button>
        <div class="small-12 columns" ngRepeat="#page in pages | async"></div>
      </div>
    </div>
  `,
    styles: [require('./pages.scss').toString()]
})
export class AdminPages {
    public pages: Observable<any[]>;
    constructor(
    ) {
    }
    ngOnInit() {

    }
    
    create() {
        
    }



}
