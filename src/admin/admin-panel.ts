import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import { AdminMenu } from './admin-menu';

@Component({
    selector: 'admin-panel',
    template: `
    <div class="admin-panel" ngClass="{{state}}">
     <div class="tab"><a (click)="toggle()"><i class="fi-play size-26"></i></a></div>    
     <h3>Admin menu</h3>
     <admin-menu></admin-menu>
    </div>
  `,
    styles: [require('./admin-panel.scss').toString()],
    directives: [ROUTER_DIRECTIVES, AdminMenu]
})

export class AdminPanel {

    public state: string;
    public view: string;
    
    constructor(
    ) {
        this.state = 'close';
    }
    ngOnInit() {

    }
    
    toggle() {
        this.state = (this.state === 'open') ? 'close' : 'open';
    }


}
