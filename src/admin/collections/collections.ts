import {Component} from 'angular2/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'admin-collections',
    template: `
    <div class="panel">
     <h4>Collections</h4>
      <div class="row">
        <div class="small-12 columns" ngRepeat="#collection in collections | async"></div>
      </div>
    </div>
  `,
    styles: [require('./collections.scss').toString()]
})
export class AdminCollections {
    public collections: Observable<any[]>;
    constructor(
    ) {
      
    }
    ngOnInit() {

    }



}
