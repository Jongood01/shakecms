import {Injectable} from 'angular2/core';
import {Router} from 'angular2/router';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from './config-service';

@Injectable()
export class User {
	public isLoggedIn: Observable<any>;

	firebaseUrl: string;
	uid: string = null;
	authData: any;
	constructor(
		private _config: ConfigService,
		private _router: Router
	) {

	}
	getProfile(authData: any) {
		var profile = { name: '', image: '' };
		switch (authData.provider) {
			case 'google':
				profile.name = authData.google.displayName;
				profile.image = authData.google.profileImageURL;
			case 'facebook':
				profile.name = authData.facebook.displayName;
				profile.image = authData.facebook.profileImageURL;
		}
		return profile;
	}
	authHandler(error: any, authData: any) {
		if (error) {
			console.log("Login Failed!", error);
		} else {

			//this.redirectToHome();
		}
	}
	newUserHandler(error: any, userData: any) {
		if (error) {
			console.log("Error creating user:", error);
		} else {
			return userData.uid;
		}
	}

}
