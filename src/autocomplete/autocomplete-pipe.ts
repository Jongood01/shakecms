import {Pipe} from 'angular2/core';

@Pipe({
  name: 'lookup'
})
export class AutocompletePipe {
  transform(value, [term]) {
    term = term || ' ';
    value = value || [];
    return value.filter((val) => val.toUpperCase().startsWith(term.toUpperCase()));
  }
}
