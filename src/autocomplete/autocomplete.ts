import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {AutocompletePipe} from './autocomplete-pipe';

@Component({
  selector: 'autocomplete',
  template: `
    <div class="autocomplete">
      <ul>
        <li *ngFor="#value of values | lookup: key; #i = index" (click)="selectSuggestion(value)">{{value}}</li>
      </ul>
    </div>
  `,
  styleUrls: ['./autocomplete/autocomplete.css'],
  pipes: [AutocompletePipe]
})
export class Autocomplete {
  @Input() key = '';
  @Input() values = [];
  @Output() update = new EventEmitter();
  public selected: string;
  public index: number = 0;
  public matches: Array<string> = [];
	constructor(
   ) {

	}
  ngOnInit() {
    this.update.emit('');
  }
  addMatch(value: string) {
    this.matches.push(value);
  }
  selectSuggestion(value: string) {
    this.selected = value;
    this.update.emit(this.selected);
  }
  suggest(event: any) {
    // Arrow down
    if (event.keyCode === 40) {

    // Arrow up
    } else if (event.keyCode === 38) {

    // Enter
    } else if(event.keyCode === 13) {

    }
  }

}
