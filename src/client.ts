import {provide, enableProdMode} from 'angular2/core';

//import {bootstrap} from 'angular2/platform/browser';
import {bootstrap} from 'angular2-universal-preview';
import {ROUTER_PROVIDERS, APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {App} from './app/app';
import {AuthService} from './services/auth-service';
import {CoreService} from './services/core-service';
import {ConfigService} from './services/config-service';
import {JwtService} from './services/jwt-service';

//enableProdMode();

bootstrap(App, [
  ConfigService,
  CoreService,
  AuthService,
  ...ROUTER_PROVIDERS,
  JwtService,
  provide(APP_BASE_HREF, { useValue: '/' } ),
  //provide(LocationStrategy, { useClass: HashLocationStrategy })
]);
