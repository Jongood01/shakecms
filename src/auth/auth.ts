import {Component} from 'angular2/core';

import { ROUTER_DIRECTIVES, RouteParams } from 'angular2/router';

@Component({
  selector: 'auth',
  template: `
  `,
  directives: [ROUTER_DIRECTIVES]
})

export class AuthComponent {
    public token: string;
	constructor(
    private _routeParams: RouteParams
    ) {
  }
  ngOnInit() {
    this.token = this._routeParams.get('token');
               
  }

}
