import {Component} from 'angular2/core';

import {
  ROUTER_DIRECTIVES
} from 'angular2/router';
import {ConfigService} from '../services/config-service';
import {AuthService} from '../services/auth-service';

@Component({
  selector: 'login',
  templateUrl: './login/login.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [AuthService]
})

export class LoginComponent {
	constructor(
    public auth: AuthService,
    public config: ConfigService
    ) {
  }
  login(type: string) {
    if (type === 'facebook') {
			this.auth.authWithFacebook();
		} else {
			this.auth.authWithGoogle();
		}
	}

}
