var webpackMerge = require('webpack-merge');
var path = require('path');

var common = {
  resolve: {
    modulesDirectories: ['node_modules'],  
    extensions: ['', '.ts', '.json', '.js']
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: [ /node_modules/ ]
      },
      {
        test: /\.scss$/,
        loaders: [ 'css', 'sass' ]
      }
    ]
  }
};

var client = {
  target: 'web',
  entry: './src/client',
  output: {
    path: path.join(__dirname, '/dist/client')
  }
};

var server = {
  target: 'node',
  entry: './src/server',
  output: {
    path: path.join(__dirname, '/dist/server')
  },
  externals: function checkNodeImport(context, request, cb) {
    if (!path.isAbsolute(request) && request.charAt(0) !== '.') {
      cb(null, 'commonjs ' + request); return;
    }
    cb();
  },
  node: {
    global: true,
    __dirname: true,
    __filename: true,
    process: true,
    Buffer: true
  }
};

var defaults = {
  context: __dirname,
  resolve: {
    root: path.join(__dirname, '/src')
  },
  output: {
    publicPath: path.resolve(__dirname),
    filename: 'bundle.js'
  }
}

module.exports = [
  // Client
  webpackMerge({}, defaults, common, client),

  // Server
  webpackMerge({}, defaults, common, server)
]
